﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class waypoints : MonoBehaviour
{
    private NavMeshAgent agent;
    public Transform Destination;
    public GameObject agentpref;
    public Transform startpoint;
    public Transform waypoints_parent;
    public Transform[] points;
    bool cango;

    // Start is called before the first frame update
    void Start()
    {
        cango = false;
        agent = GetComponent<NavMeshAgent>();
        //find waypoints parent
        waypoints_parent = GameObject.FindGameObjectWithTag("parent").transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (cango == false)
        {
            agent.SetDestination(GetClosestEnemy(points).position);
        }


    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("destination"))
        {
            //if players are > 4 reload scene 
            summoner.playercount += 1;
            if (summoner.playercount != 4)
            {

                Instantiate(agentpref, startpoint.transform.position, agentpref.transform.rotation);
            }
            else
            {
                SceneManager.LoadScene(2);
            }
            Destroy(gameObject);
        }
        //if player reached waypoints he can now go to it s final destination
        if (other.gameObject.tag.Equals("waypoint"))
        {
            Destroy(other.gameObject);
            cango = true;
            agent.SetDestination(Destination.position);
        }

    }

    /// <summary>
    /// find the closest destination to a waypoint that exists in scene
    /// </summary>
    /// <param name="points"></param>
    /// <returns></returns>
    Transform GetClosestEnemy(Transform[] points)
    {


        points = new Transform[waypoints_parent.childCount];
        for (int i = 0; i < waypoints_parent.childCount; i++)
        {

            points[i] = waypoints_parent.GetChild(i);
        }
        Transform tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;
        foreach (Transform t in points)
        {
            float dist = Vector3.Distance(t.position, currentPos);
            if (dist < minDist)
            {
                tMin = t;
                minDist = dist;
            }
        }

        return tMin;
    }
}
