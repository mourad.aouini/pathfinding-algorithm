﻿using UnityEngine;
using UnityEngine.AI;

public class resetpathing : MonoBehaviour
{

    NavMeshAgent agent;
    public NavMeshAgent agent2;
    public Transform target;
    public static bool taken;
    public GameObject trail;
    // Start is called before the first frame update

    private NavMeshPath path;
    private float elapsed = 0.0f;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        path = new NavMeshPath();
        elapsed = 0.0f;
    }

    void Update()
    {
      // 
      //  agent.SetPath(path);


        // Update the way to the goal every second.
        elapsed += Time.deltaTime;
        if (elapsed > 1.0f)
        {

            elapsed -= 1.0f;
            NavMesh.CalculatePath(transform.position, target.position, NavMesh.AllAreas, path);
            agent.SetPath(path);
        }

        for (int i = 0; i < path.corners.Length - 1; i++)

            Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "floor")
        {
            Debug.Log("done");


            //other.gameObject.GetComponent<NavMeshModifier>().area = 3;
            // other.gameObject.GetComponent<NavMeshSurface>().defaultArea=3;
            //  GameObjectUtility.SetNavMeshArea(other.gameObject, 3);
            //  other.gameObject.GetComponent<NavMeshSurface>().BuildNavMesh();
            //  agent.ResetPath();
          GameObject spawn = Instantiate(trail, other.transform.position + new Vector3(0, 1, 0), trail.transform.rotation);
            //Destroy(other.gameObject);
           Destroy(spawn, 2);

        }
    }

}
