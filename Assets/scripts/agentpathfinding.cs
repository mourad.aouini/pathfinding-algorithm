﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
public class agentpathfinding : MonoBehaviour
{

    private NavMeshAgent agent;
    public Transform Destination;
    public GameObject wallprefab;
    public GameObject agentpref;
    public Transform startpoint;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        StartCoroutine(blockwaycall());
        
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(Destination.position);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("destination"))
        {
            Debug.Log("collided");
            //if players are > 4 reload scene 
            summoner.playercount += 1;
            if (summoner.playercount != 4)
            {
                Instantiate(agentpref, startpoint.transform.position, agentpref.transform.rotation);
            }
            else
            {
                SceneManager.LoadScene(1);
            }
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Instantiate an abstacle that takes the agent position 1 second ago 
    /// </summary>
    /// <returns></returns>
    public IEnumerator blockwaycall()
    {
        yield return new WaitForSeconds(4f);
        Vector3 pos = transform.position;
        yield return new WaitForSeconds(1f);
        Instantiate(wallprefab, pos, wallprefab.transform.rotation);
    }

}
